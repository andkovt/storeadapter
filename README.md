﻿## StoreAdapter

Library for searching products and fetching product information from popular latvian e-commerce stores.

## Supported Stores

- RDElectronics

## Installation

Install required store adaptor packages from [Nuget](https://www.nuget.org/packages?q=Andkovt.StoreAdapter)

Package Manager
```bash
    Install-Package Andkovt.StoreAdapter.Store.* -Version 1.0.2
```
Dotnet CLI
```bash
    dotnet add package --version 1.0.2 Andkovt.StoreAdapter.Store.*
```

## Usage

Usage instructions can be found inside each separate store adaptor package.

### Example for RDElectronics adapter

Searching store for item named 'Test Item', selecting the first item and navigating to item's product page.

```c#
    using RdStore = Andkovt.StoreAdapter.Store.RDElectronics.Store;
    
    var store = RdStore.Create();
    var searchPage = await store.SearchAsync("Test Item");
    
    var firstItem = page.Items.First();
    var firstItemProductPage = await rdAdapter.ProductAsync(firstItem);
```