﻿using System;
using System.Linq;
using System.Threading.Tasks;
using RdStore = Andkovt.StoreAdapter.Store.RDElectronics.Store;

namespace Andkovt.StoreAdapter.Sandbox
{
    class Program
    {
        static async Task<int> Main(string[] args)
        {
            var rdAdapter = RdStore.Create();
            var page = await rdAdapter.SearchAsync("Spectre x360");

            var firstItem = page.Items.First();

            var pp = await rdAdapter.ProductAsync(firstItem);
            Console.WriteLine("Hello World!");

            return default;
        }
    }
}