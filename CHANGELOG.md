﻿# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.2] - 2021-01-16

### Fixed

- Nuget package publishing

## [1.0.1] - 2021-01-15

### Added

- NuGet package generation and publishing

## [1.0.0] - 2021-01-15

### Added

- Initial Project Structure
- StoreAdapter Base Project
- StoreAdapter Crawler for handling requests to stores
- Tests for StoreAdapter Crawler UriHelper Combine method
- Tests for UriHelper AssembleQueryString method
- Store Adapter For RDElectronics