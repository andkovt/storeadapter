﻿using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Andkovt.StoreAdapter.Crawler.Unit
{
    public class UriHelperTest
    {
        [Theory]
        [MemberData(nameof(CombineData))]
        public void Combine_UriAndStringsAsArguments_UriPathsAreCombined(string baseUri, string[] additionalPaths, string expectedResult)
        {
            Assert.Equal(expectedResult, UriHelper.Combine(new Uri(baseUri), additionalPaths).ToString());
        }

        [Theory]
        [MemberData(nameof(CombineData))]
        public void Combine_StringsAsArguments_UriPathsAreCombine(string baseUri, string[] additionalPaths, string expectedResult)
        {
            Assert.Equal(expectedResult, UriHelper.Combine(additionalPaths.Prepend(baseUri).ToArray()));
        }
        
        [Theory]
        [MemberData(nameof(AssembleQueryStringData))]
        public void AssembleQueryString_QueryParamsAsArguments_QueryIsCorrectlyAssembled(Dictionary<string, string> queryParams, string expectedResult)
        {
            Assert.Equal(expectedResult, UriHelper.AssembleQueryString(queryParams));
        }

        public static IEnumerable<object[]> CombineData()
        {
            return new List<object[]>
            {
                new object[]{"https://www.example.com", new string[] {"part1", "part2"}, "https://www.example.com/part1/part2"},
                new object[]{"https://www.example.com", new string[] {"part1"}, "https://www.example.com/part1"},
                new object[]{"https://www.example.com", new string[] {}, "https://www.example.com/"},
                new object[]{"https://www.example.com/", new string[] {}, "https://www.example.com/"},
                new object[]{"https://www.example.com", new string[] {"/part1", "/part2"}, "https://www.example.com/part1/part2"},
                new object[]{"https://www.example.com", new string[] {"/part1/", "/part2/"}, "https://www.example.com/part1/part2"},
                new object[]{"https://www.example.com", new string[] {"/part1/", "/part2/", "//"}, "https://www.example.com/part1/part2"},
                new object[]{"https://www.example.com/", new string[] {"/part1/", "/part2/", "//"}, "https://www.example.com/part1/part2"},
                new object[]{"https://www.example.com", new string[] {"\\part1\\", "\\part2\\", "//", "\\"}, "https://www.example.com/part1/part2"},
                new object[]{"https://www.example.com", new string[] {"\\"}, "https://www.example.com/"},
                new object[]{"https://www.example.com", new string[] {"\\", "\\"}, "https://www.example.com/"},
                new object[]{"https://www.example.com", new string[] {"/part1/part2/", "/part3/", }, "https://www.example.com/part1/part2/part3"},
            };
        }

        public static IEnumerable<object[]> AssembleQueryStringData()
        {
            return new List<object[]>
            {
                new object[]{new Dictionary<string, string>{{"param1", "value1"}}, "param1=value1"},
                new object[]{new Dictionary<string, string>{{"param1", "value1"}, {"param2", "value2"}}, "param1=value1&param2=value2"},
                new object[]{new Dictionary<string, string>{}, ""},
                new object[]{new Dictionary<string, string>{{"param 1", "value 1"}}, "param+1=value+1"},
                new object[]{new Dictionary<string, string>{{"param&1", "value&1"}}, "param%261=value%261"},
                new object[]{new Dictionary<string, string>{{"param[1]", "value 1"}}, "param%5b1%5d=value+1"},
            };
        }
    }
}