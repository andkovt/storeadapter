﻿using System;
using System.Threading.Tasks;
using Andkovt.StoreAdapter.Crawler;
using Andkovt.StoreAdapter.Model;
using Andkovt.StoreAdapter.Pages;
using Andkovt.StoreAdapter.Store.RDElectronics.Pages;

namespace Andkovt.StoreAdapter.Store.RDElectronics
{
    public class Store : IStore
    {
        private const string BaseUrl = "https://www.rdveikals.lv";
        private readonly ICrawler _crawler;

        public Store(ICrawler crawler)
        {
            _crawler = crawler;
            _crawler.BaseUri = new Uri(BaseUrl);
        }
        
        public string Name => "RDElectronics";
        
        public async Task<IProductPage> ProductAsync(ProductReference productReference)
        {
            var page = new ProductPage(_crawler, productReference);
            await page.ReloadAsync();
            
            return page;
        }

        public async Task<ISearchPage> SearchAsync(string searchKeyword)
        {
            return await new SearchPage(_crawler, searchKeyword).LoadPageAsync(1);
        }

        public static Store Create()
        {
            return new Store(new HttpCrawler(new BasicHttpClient()));
        }
    }
}