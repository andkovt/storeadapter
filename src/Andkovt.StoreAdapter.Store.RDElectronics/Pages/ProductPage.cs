﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Andkovt.StoreAdapter.Crawler;
using Andkovt.StoreAdapter.Crawler.Exception;
using Andkovt.StoreAdapter.Exceptions;
using Andkovt.StoreAdapter.Model;
using Andkovt.StoreAdapter.Pages;
using Andkovt.StoreAdapter.Store.RDElectronics.Helpers;

namespace Andkovt.StoreAdapter.Store.RDElectronics.Pages
{
    public class ProductPage : IProductPage
    {
        private readonly ICrawler _crawler;
        private const string ProductBaseUrl = "products/lv";

        public IEnumerable<Feature> AvailableFeatures => new[] {Feature.Name, Feature.Price};

        public ProductReference Reference { get; private set; }
        public string Name { get; private set; }
        public double Price { get; private set; }

        public ProductPage(ICrawler crawler, ProductReference reference)
        {
            _crawler = crawler;
            Reference = reference;
        }

        public async Task ReloadAsync()
        {
            var finalUrl = UriHelper.Combine(ProductBaseUrl, Reference.Id + ".html");
            Document document;

            try
            {
                document = await _crawler.NavigateAsync(finalUrl);
            }
            catch (RequestException e)
            {
                if (e.StatusCode == HttpStatusCode.NotFound)
                    throw new ProductNotFoundException(Reference);
                throw;
            }

            Name = ProductPageHelper.ExtractTitle(document);
            Price = ProductPageHelper.ExtractPrice(document);
        }
    }
}