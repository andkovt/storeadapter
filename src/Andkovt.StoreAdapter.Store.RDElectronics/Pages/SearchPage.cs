﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using Andkovt.StoreAdapter.Crawler;
using Andkovt.StoreAdapter.Crawler.Exception;
using Andkovt.StoreAdapter.Model;
using Andkovt.StoreAdapter.Pages;
using Andkovt.StoreAdapter.Store.RDElectronics.Helpers;

namespace Andkovt.StoreAdapter.Store.RDElectronics.Pages
{
    public class SearchPage : ISearchPage
    {
        private const string SearchBaseUrl = "search/lv";
        
        private readonly ICrawler _crawler;

        private bool _firstLoad = true;

        public SearchPage(ICrawler crawler, string searchKeyword)
        {
            _crawler = crawler;
            Keyword = searchKeyword;
        }

        public IEnumerable<Feature> AvailableFeatures => new[] {Feature.Name, Feature.Price};
        public string Keyword { get; private set; }
        public int Page { get; private set; } = 1;
        public int TotalPages { get; private set; } = 1;
        public int TotalItems { get; private set; } = 0;
        public IEnumerable<ProductReference> Items { get; private set; }
        
        public async Task<ISearchPage> NextPageAsync()
        {
            return await LoadPageAsync(Page + 1);
        }

        public async Task<ISearchPage> PreviousPageAsync()
        {
            return await LoadPageAsync(Page - 1);
        }

        public async Task<ISearchPage> LoadPageAsync(int pageNumber)
        {
            if (pageNumber < 0)
                throw new CrawlerException("Search page number could not be less than 1");
            if (pageNumber > TotalPages)
                throw new CrawlerException($"Can't load page '{pageNumber}'. Total pages: '{TotalPages}'");
            
            var url = AssembleSearchUrl(Keyword, pageNumber);
            await LoadAndProcessUrlAsync(url, pageNumber);

            return this;
        }
        
        public async Task ReloadAsync()
        {
            _firstLoad = true;
            await LoadPageAsync(Page);
        }
        
        private string AssembleSearchUrl(string keyword, int page = 1)
        {
            return UriHelper.Combine(SearchBaseUrl, "word", HttpUtility.UrlEncode(keyword), "page", page.ToString());
        }
        
        private async Task LoadAndProcessUrlAsync(string url, int pageNumber)
        {
            var document = await _crawler.NavigateAsync(url);
            Page = pageNumber;

            if (_firstLoad)
            {
                TotalItems = SearchPageHelper.ExtractTotalItems(document);
                TotalPages = SearchPageHelper.HasPagination(document)
                    ? TotalPages = SearchPageHelper.ExtractTotalPages(document)
                    : 1;
                
                _firstLoad = false;
            }

            Items = SearchPageHelper.ExtractItems(document);
        }
    }
}