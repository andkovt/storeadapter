﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Andkovt.StoreAdapter.Crawler;
using Andkovt.StoreAdapter.Model;

namespace Andkovt.StoreAdapter.Store.RDElectronics.Helpers
{
    public static class SearchPageHelper
    {
        private const string TotalItemsSelector = "//p[@class=\"filters-top__text text--lead search_products_count\"]";
        private const string TotalItemsRegex = "(\\d*) rezultāti";
        
        private const string PaginatorContainerSelector = "//div[@id=\"main_container_wrapper\"]//div[@class=\"group group--tiny group--rounded\"]";
        private const string ProductSelector = "//div[@id=\"main_container_wrapper\"]//div[@class=\"product__info\"]";
        
        public static int ExtractTotalItems(Document document)
        {
            var totalText = document.GetText(TotalItemsSelector);
            var match = Regex.Match(totalText, TotalItemsRegex);
            
            return int.Parse(match.Groups[1].Value);
        }

        public static bool HasPagination(Document document)
        {
            return document.HasNode(PaginatorContainerSelector);
        }

        public static int ExtractTotalPages(Document document)
        {
            return int.Parse(document.GetText(
                PaginatorContainerSelector + "//a",
                Document.QuerySelection.Last
            ));
        }

        public static IEnumerable<ProductReference> ExtractItems(Document document)
        {
            var items = new List<ProductReference>();
            var itemNodes = document.Query(ProductSelector);
            foreach (var itemNode in itemNodes)
            {
                var aNode = itemNode.ChildNodes.FindFirst("a");
                var reference = aNode.GetAttributeValue("href", "");
                var name = aNode.InnerText;

                reference = reference.Replace("products/lv/", "");
                reference = reference.Replace(".html", "");
                var splitName = name.Split("\r\n", StringSplitOptions.TrimEntries | StringSplitOptions.RemoveEmptyEntries);

                name = splitName.Length == 2 ? splitName[0] : name;
                items.Add(new ProductReference
                {
                    Id = reference,
                    Name = name,
                });
            }

            return items;
        }
    }
}