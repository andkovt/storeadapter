﻿using System;
using Andkovt.StoreAdapter.Crawler;

namespace Andkovt.StoreAdapter.Store.RDElectronics.Helpers
{
    public static class ProductPageHelper
    {
        private const string ItemNameSelector = "//h1";
        private const string ItemPriceSelector = "//span[@itemprop=\"price\"]";

        public static string ExtractTitle(Document document)
        {
            return document.GetTextFirst(ItemNameSelector);
        }
        
        public static double ExtractPrice(Document document)
        {
            var price = document.GetAttributeValue(ItemPriceSelector, "content");
            price = price.Replace(".", ",");
            
            return double.Parse(price);
        }
    }
}