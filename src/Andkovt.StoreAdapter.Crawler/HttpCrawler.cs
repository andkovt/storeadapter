﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Andkovt.StoreAdapter.Crawler.Exception;
using HtmlAgilityPack;

namespace Andkovt.StoreAdapter.Crawler
{
    public class HttpCrawler : ICrawler
    {
        private readonly IHttpClient _client;

        public HttpCrawler(IHttpClient client)
        {
            _client = client;
        }

        public Uri BaseUri { get; set; }

        public async Task<Document> NavigateAsync(string path, IDictionary<string, string> query = null)
        {
            var uri = AssembleUri(path, query);
            var response = await _client.GetAsync(uri);

            if (!response.IsSuccessStatusCode)
                throw new RequestException(uri, response.StatusCode, response.ReasonPhrase);

            var responseContent = await response.Content.ReadAsStringAsync();
            var parsedDocument = new HtmlDocument();
            parsedDocument.LoadHtml(responseContent);
            
            return new Document(parsedDocument);
        }

        private Uri AssembleUri(string path, IDictionary<string, string> query = null)
        {
            var urlPath = BaseUri == null ? path : UriHelper.Combine(BaseUri.ToString(), path);
            if (query != null)
                urlPath += "?" + UriHelper.AssembleQueryString(query);

            return new Uri(urlPath);
        }
    }
}