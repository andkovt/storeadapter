﻿#nullable enable
namespace Andkovt.StoreAdapter.Crawler.Exception
{
    public class DocumentException : System.Exception
    {
        public DocumentException(string? message) : base(message)
        {
        }

        public DocumentException(string? message, System.Exception? innerException) : base(message, innerException)
        {
        }
    }
}