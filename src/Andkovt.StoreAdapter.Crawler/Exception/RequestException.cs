﻿#nullable enable
using System;
using System.Net;

namespace Andkovt.StoreAdapter.Crawler.Exception
{
    public class RequestException : CrawlerException
    {
        public RequestException(Uri requestedUri, HttpStatusCode responseCode, string? message) : base(message)
        {
            Uri = requestedUri;
            StatusCode = responseCode;
        }

        public Uri Uri { get; }
        public HttpStatusCode StatusCode { get; }
    }
}