﻿#nullable enable
namespace Andkovt.StoreAdapter.Crawler.Exception
{
    public class CrawlerException : System.Exception
    {
        public CrawlerException(string? message) : base(message)
        {
        }

        public CrawlerException(string? message, System.Exception? innerException) : base(message, innerException)
        {
        }
    }
}