﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Andkovt.StoreAdapter.Crawler
{
    public class BasicHttpClient : IHttpClient
    {
        private readonly HttpClient _client;
        
        public BasicHttpClient()
        {
            _client = new HttpClient();
        }
        
        public async Task<HttpResponseMessage> GetAsync(Uri path)
        {
            return await _client.GetAsync(path);
        }
    }
}