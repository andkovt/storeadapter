﻿using System.Linq;
using Andkovt.StoreAdapter.Crawler.Exception;
using HtmlAgilityPack;

namespace Andkovt.StoreAdapter.Crawler
{
    public class Document
    {
        public HtmlDocument Raw { get; }

        public Document(HtmlDocument rawDocument)
        {
            Raw = rawDocument;
        }

        public string GetTextFirst(string xpath)
        {
            return GetText(xpath, true);
        }

        public string GetText(string xpath, QuerySelection selection = QuerySelection.OnlyOne)
        {
            var node = QueryOne(xpath, selection);
            return node.InnerText;
        }

        public bool HasNode(string xpath)
        {
            return Raw.DocumentNode.SelectSingleNode(xpath) != null;
        }

        public string GetAttributeValue(string xpath, string attributeName)
        {
            var node = QueryOne(xpath, QuerySelection.First);
            var attribute = node.Attributes.FirstOrDefault(a => a.Name == attributeName);
            if (attribute == null)
                throw new DocumentException(
                    $"Node matching xpath '{xpath}' does not have an attribute '{attributeName}'");

            return attribute.Value;
        }

        public HtmlNode QueryOne(string xpath, QuerySelection selection = QuerySelection.OnlyOne)
        {
            var nodes = Query(xpath);
            if (nodes.Count > 1 && selection == QuerySelection.OnlyOne)
                throw new DocumentException($"More than one node matched xpath '{xpath}'.");

            return selection == QuerySelection.Last ? nodes.Last() : nodes.First();
        }

        public HtmlNodeCollection Query(string xpath)
        {
            var nodes = Raw.DocumentNode.SelectNodes(xpath);
            if (nodes == null)
                throw new DocumentException($"No nodes matched xpath '{xpath}'");

            return nodes;
        }
        
        private string GetText(string xpath, bool useFirst)
        {
            var nodes = Raw.DocumentNode.SelectNodes(xpath);
            if (nodes == null)
                throw new DocumentException($"No nodes matched xpath '{xpath}'");
            if (nodes.Count > 1 && !useFirst)
                throw new DocumentException(
                    $"More than one node matched xpath '{xpath}'. Use GetTextFirst to select first node");

            return nodes.First().InnerText;
        }
        
        public enum QuerySelection
        {
            OnlyOne,
            First,
            Last
        }
    }
}