﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Andkovt.StoreAdapter.Crawler
{
    public static class UriHelper
    {
        public static Uri Combine(Uri uri, params string[] additionalPaths)
        {
            return new Uri(Combine(additionalPaths.Prepend(uri.ToString()).ToArray()));
        }

        public static string Combine(params string[] paths)
        {
            var basePart = paths.FirstOrDefault();
            if (basePart == null) return "";
            
            foreach (var path in paths.Skip(1))
                basePart = Append(basePart, path);

            if (!basePart.StartsWith("http"))
                return basePart;
            
            return new Uri(basePart).ToString();
        }

        public static string AssembleQueryString(IDictionary<string, string> queryParams)
        {
            var queryParts = queryParams.Keys.Select(queryParam =>
                $"{HttpUtility.UrlEncode(queryParam)}={HttpUtility.UrlEncode(queryParams[queryParam])}");

            return string.Join("&", queryParts);
        }

        private static string Append(string basePart, string additionalPart)
        {
            basePart = basePart.TrimEnd('/', '\\');
            additionalPart = additionalPart.Trim('/', '\\');

            if (string.IsNullOrEmpty(additionalPart))
                return basePart;

            return $"{basePart}/{additionalPart}";
        }
    }
}