﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Andkovt.StoreAdapter.Crawler
{
    public interface IHttpClient
    {
        Task<HttpResponseMessage> GetAsync(Uri path);
    }
}