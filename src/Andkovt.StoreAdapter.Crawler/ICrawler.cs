﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Andkovt.StoreAdapter.Crawler
{
    public interface ICrawler
    {
        Uri BaseUri { get; set; }

        Task<Document> NavigateAsync(string path, IDictionary<string, string> query = null);
    }
}