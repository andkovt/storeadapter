﻿using System;
using Andkovt.StoreAdapter.Pages;

namespace Andkovt.StoreAdapter.Exceptions
{
    public class UnsupportedFeatureException : StoreAdapterException
    {
        public UnsupportedFeatureException(Feature feature, PageType pageType) : base(
            $"Feature '{FeatureToString(feature)}' is not supported on '{PageTypeToString(pageType)}' page")
        {
        }

        private static string FeatureToString(Feature feature)
        {
            return Enum.GetName(feature);
        }

        private static string PageTypeToString(PageType pageType)
        {
            return Enum.GetName(pageType);
        }
    }
}