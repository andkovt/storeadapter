﻿using Andkovt.StoreAdapter.Model;

namespace Andkovt.StoreAdapter.Exceptions
{
    public class ProductNotFoundException : StoreAdapterException
    {
        public ProductNotFoundException(ProductReference reference) : base($"Product '{reference.Id}' not found")
        {
            
        }
    }
}