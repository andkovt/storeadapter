﻿#nullable enable
using System;

namespace Andkovt.StoreAdapter.Exceptions
{
    public class StoreAdapterException : Exception
    {
        public StoreAdapterException(string? message) : base(message)
        {
            
        }

        public StoreAdapterException(string? message, Exception? innerException) : base(message, innerException)
        {
        }
    }
}