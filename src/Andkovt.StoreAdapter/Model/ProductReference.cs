﻿namespace Andkovt.StoreAdapter.Model
{
    public class ProductReference
    {
        public int Version { get; protected set; } = 1;
        public string Id { get; set; }
        public string Name { get; set; }
    }
}