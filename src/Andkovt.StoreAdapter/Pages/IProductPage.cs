﻿using Andkovt.StoreAdapter.Model;

namespace Andkovt.StoreAdapter.Pages
{
    public interface IProductPage : IStorePage
    {
        public ProductReference Reference { get; }
        public string Name { get; }
        public double Price { get; }
    }
}