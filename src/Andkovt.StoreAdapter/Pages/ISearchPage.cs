﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Andkovt.StoreAdapter.Model;

namespace Andkovt.StoreAdapter.Pages
{
    public interface ISearchPage : IStorePage
    {
        string Keyword { get; }
        int Page { get; }
        int TotalPages { get; }
        int TotalItems { get; }
        
        IEnumerable<ProductReference> Items { get; }

        Task<ISearchPage> NextPageAsync();
        Task<ISearchPage> PreviousPageAsync();
        Task<ISearchPage> LoadPageAsync(int pageNumber);
    }
}