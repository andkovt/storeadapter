﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Andkovt.StoreAdapter.Pages
{
    public interface IStorePage
    {
        IEnumerable<Feature> AvailableFeatures { get; }
        Task ReloadAsync();
    }
}