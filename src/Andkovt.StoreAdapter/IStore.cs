﻿using System.Threading.Tasks;
using Andkovt.StoreAdapter.Model;
using Andkovt.StoreAdapter.Pages;

namespace Andkovt.StoreAdapter
{
    public interface IStore
    {
        string Name { get; }
        
        Task<IProductPage> ProductAsync(ProductReference productReference);
        Task<ISearchPage> SearchAsync(string searchKeyword);
    }
}